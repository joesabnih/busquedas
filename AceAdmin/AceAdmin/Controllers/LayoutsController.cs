﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AceAdmin.Controllers
{
    public class LayoutsController : Controller
    {
        //
        // GET: /Layouts/

        public PartialViewResult TopMenu()
        {
            return PartialView("_TopMenu");
        }

        public PartialViewResult TopMenu1()
        {
            return PartialView("_TopMenu1");
        }

        public PartialViewResult TopMenu3()
        {
            return PartialView("_TopMenu3");
        }

        public PartialViewResult TopMovilMenu()
        {
            return PartialView("_TopMovilMenu");
        }

        public PartialViewResult TopMovilMenu2()
        {
            return PartialView("_TopMovilMenu2");
        }

        public PartialViewResult TopMovilMenu3()
        {
            return PartialView("_TopMovilMenu3");
        }
    }
}
