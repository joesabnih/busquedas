﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AceAdmin.Controllers
{
    public class MorePagesController : Controller
    {
        //
        // GET: /MorePages/

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Inbox()
        {
            return View();
        }

        public ActionResult Pricing()
        {
            return View();
        }

        public ActionResult Invoice()
        {
            return View();
        }

        public ActionResult TimerLine()
        {
            return View();
        }

        public ActionResult Email()
        {
            return View();
        }

        public PartialViewResult Login()
        {
            return PartialView("_Login");
        }

        public PartialViewResult EmailConfirmation()
        {
            return PartialView("_EmailConfirmation");
        }

        public PartialViewResult EmailNavbar()
        {
            return PartialView("_EmailNavbar");
        }

        public PartialViewResult EmailNewsletter()
        {
            return PartialView("_EmailNewsletter");
        }

        public PartialViewResult EmailContrast()
        {
            return PartialView("_EmailContrast");
        }

    }
}
