﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AceAdmin.Controllers
{
    public class OtherPagesController : Controller
    {
        //
        // GET: /OtherPages/

        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Error404()
        {
            return View();
        }
        public ActionResult Error500()
        {
            return View();
        }
        public ActionResult Grid()
        {
            return View();
        }
        public ActionResult Blank()
        {
            return View();
        }

    }
}
