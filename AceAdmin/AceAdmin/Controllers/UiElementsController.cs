﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AceAdmin.Controllers
{
    public class UiElementsController : Controller
    {
        //
        // GET: /UiElements/

        public ActionResult Typography()
        {
            return View();
        }

        public ActionResult Elements()
        {
            return View();
        }

        public ActionResult Buttons()
        {
            return View();
        }

        public ActionResult Sliders()
        {
            return View();
        }

        public ActionResult TreeViews()
        {
            return View();
        }

        public ActionResult JqueryUi()
        {
            return View();
        }

        public ActionResult Nestable()
        {
            return View();
        }


    }
}
