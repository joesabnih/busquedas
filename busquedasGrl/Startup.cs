﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(busquedasGrl.Startup))]
namespace busquedasGrl
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
